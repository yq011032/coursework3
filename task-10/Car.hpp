#pragma once
#include <string>
using namespace std;
class Car {
public: // visibility, details taught later
   // DATA
    int registration;
    string make;
    // METHODS, i.e., functions on objects
    int getRegistration() { return registration; }
    string getmake() { return make; }

    void setRegistration(int pReg) {
        registration = pReg;
    }
    void setmake(string pmake) {
        make = pmake;
    }

};
