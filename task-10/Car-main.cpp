#include <iostream>
#include "Car.hpp"
using namespace std;
int main() {
	/*
	Car car1; // creates a new instance
	car1.setmake("audi");
	Car car2; // creates a new instance
	car2.setmake("bmw");
	// access to data is possible with the "." punctuator
	cout << "car1 make is: " << car1.make << endl;
	cout << "car2 make is: " << car2.getmake() << endl;
	return 0;
}
*/
	Car* car1;
	car1 = new Car(); // instantiating a new car using new operator
	car1->setmake("audi");
	cout << "car1 make is: " << car1->make << endl;
	delete car1; // freeing car
	return 0;
}


	