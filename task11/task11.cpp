#include <iostream>
#include <string>
using namespace std;

class Car {       // The class
public:             // Access specifier
    string model;        // Attribute (string variable)
    string color;  // Attribute (string variable)
    int year; // Attribute (integer variable)
    int accommodation(int maxAccommodation); //declaring function
};
int Car::accommodation(int maxAccommodation) { //accessing the function
    return maxAccommodation;
}
int main() {
    Car myCar;  // Create an object of Car

    // Access attributes and set values
    myCar.model = "Mazda";
    myCar.color = "Black";
    myCar.year = 2010;

    // Print values
    cout << "My car is " << myCar.model << ". " << "Its color is " << myCar.color << ". " << "It is registered in " << myCar.year << endl; //printing variables
    cout << "My car's maximum accommodation including driver is " << myCar.accommodation(5) << endl; //printing the function
    return 0;
}
