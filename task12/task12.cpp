// task12.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include<iostream>
using namespace std;
class MyPen {  // class
public: // access specifier
    MyPen() { //constructor
        cout << "introducing the constructor";
    }
};
int main() {
    MyPen ballPen; //create an object of my class MyPen
    return 0;
}
