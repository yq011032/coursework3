// task9.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include<iostream>
#include<string>
using namespace std;

class Car {
public:
	string model; // string variable (attribute)
	string color; // string variable (attribute)
	int year; // integer variable (attribute)
};

int main() {
	Car myCar; // creating an object of car 

	// Accessing attributes and setting values
	myCar.model = "Mazda";
	myCar.color = "Black";
	myCar.year = 2010;

	cout << "The car is " << myCar.model << "." << " It's color is " << myCar.color << "." << " It's made in " << myCar.year << endl;
	return 0; // output, printing values 
}
