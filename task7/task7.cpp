// task7.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>
using namespace std;

	void printInputType(int type)
	{
		cout << "integer";
	}
	void printInputType(string type)
	{
		cout << "string";
	} // This is called overloading funnction as we can see that we have the same function 
	// but with different parameter types. There are "integer data type" in the first function and "string data type" in 
// the second function. Therefore, we call them overloading function.