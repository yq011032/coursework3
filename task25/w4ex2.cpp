#include <iostream>
using namespace std;

class base {
public:
virtual void print() { // the virtual fuction would access the objects of the derived class
cout << "print base class" << endl;

	}
void show() {
cout << "show base class" << endl;

	}

};
class derived :public base {
public:
void print() {
cout << "print derived class" << endl;

	}
void show() {
cout << "show derived class" << endl;

	}

};


int main() {
base * bptr; // declaring new pointer bptr to the base class 
derived d;
bptr = &d; // this is a reference to the base class 
bptr->print(); // it will still print out the derived class 
bptr->show(); // printing the show in the base class
return 0;

}
