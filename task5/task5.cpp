#include <iostream>
using namespace std;
void swap(int& first, int& second) {
	int temp;
	temp = first;
	first = second;
	second = temp;
}
int main() {
	int x = 5;
	int y = 10;
	swap(x, y);
	// It will swap the value of x with the value of y 
	cout << "x = " << x << endl << "y = " << y << endl; // Output, x will be equal to 10 and y equal to 5
	return 0;
}