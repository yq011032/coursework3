class Q : public P {
public:
 void print() { cout << " Inside Q"; }

};

class R : public Q { };

int main(void)
{
R r; // declaring new object r to the class R 
r.print(); // setting the new object r to its parent class which is class Q
// it is printing the class Q because class Q is the parent class
return 0;
}
