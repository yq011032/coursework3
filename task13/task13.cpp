// task13.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

class MyNumber { // class
public: // public access specifier
    int a; // public attribute
private: // private access specifier
    int b; //private attribute
}
};

int main() {
    MyNumber num1;
    num1.a = 5; // allowed to access
    num1.b = 6; // not allowed to access, out put would be b is private
    return 0;
}